# GIT commands
Create SSH key with comment
ssh-keygen -t rsa -C "Comment for key"

You can add a new remote by executing the git remote add NAME URL command, for example, 
git remote add origin git@gitlab.com:example/example.git

